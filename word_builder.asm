data segment 
    txt1 db "          Hello there!        "
    txt2 db "    and welcome to my game!   "
    txt3 db "in this game, you will have to build words"
    txt4 db "    using the given letters   "
    txt5 db "        Are you ready?        "
    txt6 db "  press enter to continue...  "                          
    txt7 db "   This is the next window    "
    txt8 db " (navigate using space button)" 
    txt9 db "_"                              
    txt10 db "    Your given letters are:   "
    txt11 db "           N,I,K,S            "
    txt12 db "Your task is to create 4 words"
    txt13 db "      using those letters     "
    txt14 db "Reminder! Your letters are N,I,K,S"
    txt15 db "   It does not matter if the  "
    txt16 db "   letters are small or big   "
    txt17 db "  Write your choices in this  "
    txt18 db "           text box           "  
    txt19 db "--------------"  
    txt20 db "|"              
    txt21 db "-----------------" 
    txt22 db "Your answer:"
    txt23 db "v"
    txt24 db "  Hit Enter when you are done "
    txt25 db "You won"
    exc db "!"
    s db "s"
    k db "k"
    i db "i"
    n db "n"
    tmp dw ?
    tmp2 db ?
    ctemp db ?
    counter db 0 
    pointer db " "  
    delword db "               "  
    errormsg db "Error! not a word!"
    clearerror db "                  "
    ar db ?,?,?,?
    scorear db ?,?,?,?
    
    
ends

stack segment
    dw   128  dup(0)
ends

code segment
start:
  mov ax,@data
  mov ds,ax 
  mov es,ax
  
  mov ah, 13h
  mov bp, offset txt8
  mov bh, 0
  mov bl, 14
  mov cx, 30
  mov dl, 24
  int 10h
  
  mov bp,offset txt1
  mov bl,11
  mov cx,30
  mov dh,11
  int 10h 
  
  mov bp, offset txt2
  mov dh, 13
  int 10h
  mov ah, 2
  mov dh, 14
  mov dl, 39
  int 10h
  
  
  call waitforspace
  
  mov cx, 3  
  mov dh, 11
  call delete
  
  mov bp, offset txt3
  mov cx, 42
  mov dl, 18
  mov dh, 11
  int 10h
  
  mov bp, offset txt4
  mov dh, 13 
  mov dl, 24
  mov cx, 30
  int 10h
  mov ah, 2
  mov dh, 14
  mov dl, 39
  int 10h
  
  call waitforspace
  
  mov cx, 3
  mov dh, 11
  call delete
  
  mov cx,1
  mov dh, 0
  call delete
  
  mov bp, offset txt5
  mov cx, 30
  mov dl, 24
  mov dh, 11
  int 10h
  
  mov bp, offset txt6
  mov dh, 14 
  mov bl, 15
  int 10h
  mov ah, 2
  mov dh, 16
  mov dl, 39
  int 10h     
  
  call waitforenter
  
  mov cx, 4
  mov dh, 11
  call delete
  
  ;Level 1;
  
  ;Preperation;
  mov bp, offset txt8
  mov dh, 0
  mov dl, 49
  mov bl, 14
  mov cx, 30
  int 10h
  
  ;Layout print
  mov cx, 4
  mov tmp, cx  
  mov ah,13h
  mov bp,offset txt9
  mov bh,0
  mov bl,15
  mov dl,38
  mov dh,6
  loop2:
  mov tmp, cx 
  mov cx, 1
  int 10h
  mov cx, tmp
  inc dh
  loop loop2
  
  mov dl, 40
  mov dh, 7
  mov cx, 1
  int 10h
  
  mov dl, 42
  int 10h
  
  mov dl, 36
  mov dh, 8
  int 10h
  
  mov dl, 34
  int 10h
  
  mov dl, 36
  mov dh, 9
  int 10h
  
  mov dl, 38
  int 10h
  
  mov dl, 40
  int 10h  
  
  mov dl, 42
  int 10h
  
  mov ah, 13h 
  mov bp, offset txt10
  mov bh, 0
  mov bl, 12
  mov cx, 30
  mov dl, 24
  mov dh, 13
  int 10h
  
  mov bp, offset txt11
  mov dh, 15
  int 10h
  
  mov dh, 17
  mov dl, 39
  mov ah, 2
  int 10h
  
  call waitforspace
  mov cx, 3
  mov dh, 13
  call delete
  
  mov ah, 13h
  mov bp, offset txt15
  mov dh, 13
  mov dl, 24
  mov cx, 30
  int 10h
  
  mov bp, offset txt16
  mov dh, 15
  int 10h
  
  mov bp, offset txt14
  mov dh, 24
  mov dl, 46 
  mov bl, 13 
  mov cx, 34
  int 10h
  
  mov ah, 2
  mov bh, 0
  mov dh, 17
  mov dl, 39
  int 10h
  call waitforspace
  
  mov dh, 13
  mov cx, 3
  call delete
  
  mov bl, 11
  mov bp, offset txt17
  mov dh, 13
  mov dl, 24
  mov cx, 30
  int 10h
  
  mov bp, offset txt18
  mov dh, 15
  int 10h
  
  mov bp, offset txt24
  mov dh, 18
  mov bl, 10
  int 10h
  
  ;print textbox and everything till 349
  mov bp, offset txt19
  mov bl, 11
  mov cx, 14
  mov dl, 10
  mov dh, 13
  int 10h
  
  mov cx, 4
  mov tmp, cx  
  mov ah,13h
  mov bp,offset txt20
  mov bh,0
  mov dl,10
  mov dh,14
  loop3:
  mov tmp, cx 
  mov cx, 1
  int 10h
  mov cx, tmp
  inc dh
  loop loop3 
  
  mov bp, offset txt23
  mov dh, 17
  mov cx, 1
  int 10h
  
  mov bp, offset txt21  
  mov cx, 17
  mov bl, 8
  mov dl, 2
  mov dh, 18
  int 10h
  
  mov tmp2, 2
  mov dl, 1
  cont1: 
  dec tmp2
  mov cx, 5
  mov tmp, cx  
  mov ah,13h
  mov bp,offset txt20
  mov bh,0
  mov dh,19
  loop4:
  mov tmp, cx 
  mov cx, 1
  int 10h
  mov cx, tmp
  inc dh
  loop loop4
  mov dl, 19 
  cmp tmp2, 1
  je cont1
  
  mov bp, offset txt21  
  mov cx, 17
  mov bl, 8
  mov dl, 2
  mov dh, 24
  int 10h 
  
  mov bl, 136
  mov dh, 19
  int 10h 
  mov dh, 23
  int 10h
  mov bp, offset txt9
  mov cx, 1
  mov dh, 20
  int 10h
  mov dh, 21
  int 10h
  mov dh, 22
  int 10h
  mov dl, 18
  int 10h
  mov dh, 21
  int 10h 
  mov dh, 20
  int 10h
  
  mov bp, offset txt22
  mov cx, 12
  mov bl, 15
  mov dh, 20
  mov dl, 4
  int 10h
  
  mov bp, offset pointer
  mov cx, 0
  mov dh, 22
  mov dl, 8
  int 10h 
  
  
  call inputword 
  retc: ;always jumps here after pressing enter
  call checkifcorrect
  call deleteword 
  call cleararray
  call errorclear
  call checkifwon
  cmp dl, 100
  je endgame
  call inputword                       
  
  ;print win message
  endgame:
  mov ah, 0
  int 10h
  mov ah, 13h
  mov bp, offset txt25
  mov bh, 0
  mov bl, 10
  mov cx, 7
  mov dl, 15
  mov dh, 12
  int 10h 
  
  mov bp, offset exc
  mov dl, 22
  loopin:
  mov cx, 1
  mov bl, 10
  int 10h
  int 10h
  mov bl, 0
  int 10h
  int 10h 
  inc dl
  cmp dl, 24
  je contin
  mov cx, 2
  loop loopin
  contin:
  mov dl, 22
  mov cx, 2
  loop loopin
  
  jmp end                  

;--------------------------------------------------------------;
;--------------------------------------------------------------;  
;--------------------------------------------------------------;
   
  ;---Procedures---;
  delete PROC
    ;procedure to delete rows by number and place
    ;cx = number of rows
    ;dh = starting Y
    mov ctemp, bl  
    loop1:
    mov tmp, cx
    mov ah,13h
    mov bp,offset txt3
    mov bh,0
    mov bl,0
    mov cx,42
    mov dl,18
    int 10h
    inc dh
    mov cx, tmp
    loop loop1
    mov bl, ctemp  
    ret
  delete ENDP
  
  deleteword PROC
    ;procedure to delete word input after entered
    mov  ah, 13h
    mov  bp, offset delword
    mov  bh, 0
    mov  bl, 15
    mov  cx, 15
    mov  dl, 3 
    mov  dh, 22 
    int  10h
    mov ah, 2
    mov dh, 22
    mov dl, 8 
    mov bh, 0
    int 10h
    ret
  deleteword ENDP
  
  waitforspace PROC
    mov tmp2, ah
    mov ah, 08h
    test1:
    int 21h
    cmp al, 20h
    jne test1
    mov ah, tmp2
    ret
  waitforspace ENDP
  
  waitforenter PROC
    mov tmp2, ah
    mov ah, 08h
    test2:
    int 21h
    cmp al, 0Dh
    jne test2 
    mov ah, tmp2
    ret
  waitforenter ENDP 
  
  checkletterlevel1 PROC 
    ;procedure to check if the input letter is from the list
    checklevel1start:
    cmp al, 73h ;s
    je conti
    cmp al, 53h ;S
    je conti
    cmp al, 69h ;i
    je conti
    cmp al, 49h ;I
    je conti
    cmp al, 6Eh ;n
    je conti
    cmp al, 4Eh ;N
    je conti
    cmp al, 6Bh ;k
    je conti
    cmp al, 4Bh ;K
    je conti
    cmp al, 0Dh ;Enter
    je retc 
    jmp nconti
    conti:
    mov dl, al
    mov ah, 2 
    mov bl, 0
    mov cl, 0
    int 21h
    ret
    nconti:
    mov ah, 7
    int 21h
    jmp checklevel1start
  checkletterlevel1 ENDP
  
  sink PROC
    ;procedure to print "sink"
    mov al, 0
    mov bh, 0
    mov ah, 13h 
    mov ctemp, bl
    mov bl, 15
    mov cx, 1    
    mov bp, offset s
    mov dl, 38
    mov dh, 6
    int 10h
    mov bp, offset i
    mov dh, 7
    int 10h
    mov bp, offset n
    mov dh, 8
    int 10h  
    mov bp, offset k
    mov dh, 9
    int 10h
    mov bl, ctemp
    mov di, offset scorear
    mov [di], 1
    ret
  sink ENDP 
  
  ink PROC
    ;procedure to print "ink"
    mov al, 0
    mov bh, 0
    mov ah, 13h
    mov ctemp, bl
    mov bl, 15
    mov cx, 1    
    mov bp, offset i
    mov dl, 38
    mov dh, 7
    int 10h
    mov bp, offset n
    mov dl, 40
    int 10h
    mov bp, offset k
    mov dl, 42
    int 10h
    mov bl, ctemp
    mov di, offset scorear+1 
    mov [di], 1
    ret
  ink ENDP
    
  sin PROC
    ;procedure to print "sin"
    mov al, 0
    mov bh, 0
    mov ah, 13h
    mov ctemp, bl
    mov bl, 15
    mov cx, 1    
    mov bp, offset s
    mov dl, 34
    mov dh, 8
    int 10h
    mov bp, offset i
    mov dl, 36
    int 10h
    mov bp, offset n
    mov dl, 38
    int 10h
    mov bl, ctemp
    mov di, offset scorear+2
    mov [di], 1
    ret
  sin ENDP
    
  skin PROC
    ;procedure to print "skin" 
    mov al, 0
    mov bh, 0
    mov ah, 13h
    mov ctemp, bl
    mov bl, 15
    mov cx, 1    
    mov bp, offset s
    mov dl, 36
    mov dh, 9
    int 10h
    mov bp, offset k
    mov dl, 38
    int 10h
    mov bp, offset i
    mov dl, 40
    int 10h  
    mov bp, offset n
    mov dl, 42
    int 10h
    mov bl, ctemp
    mov di, offset scorear+3 
    mov [di], 1
    ret
  skin ENDP
  
  inputletter PROC
    ;simple proc to letter input + check
    mov ah, 7
    int 21h
    call checkletterlevel1
    ret                          
  inputletter ENDP
  
  inputword PROC
    ;proc to word input  
    mov si, offset ar
    mov cx, 3 
    loop5:
    call inputletter
    mov [si], al
    inc si
    loop loop5
    ret
  inputword ENDP
  
  checkifcorrect PROC
    ;proc to check if the word input is correct or not
    mov si, offset ar
    cmp [si], 73h  ;s
    je checksink1
    cmp [si], 53h  ;S
    je checksink1
    ;first letter is not s, jmp check "ink"
    jmp checkink1
    checksink1: 
    inc si
    cmp [si], 69h  ;i
    je checksink2
    cmp [si], 49h  ;I
    je checksink2
    jmp checkskin
    checksink2:
    inc si
    cmp [si], 6Eh  ;n
    je checksink3
    cmp [si], 4Eh  ;N
    je checksink3
    jmp errorcont
    checksink3:
    inc si
    cmp [si], 6Bh  ;k
    je printsink
    cmp [si], 4Bh  ;K  
    je printsink   ;the word is sink, print it
    cmp [si], 0
    je printsin    ;the word is sin, print it
    jmp errorcont
    printsink:
    call sink
    ret
    printsin:
    call sin 
    ret
    checkskin:
    cmp [si], 6Bh  ;k
    je checkskin1
    cmp [si], 4Bh  ;K
    je checkskin1
    jmp errorcont
    checkskin1:
    inc si
    cmp [si], 69h  ;i
    je checkskin2
    cmp [si], 49h  ;I
    je checkskin2
    jmp errorcont
    checkskin2:
    inc si
    cmp [si], 6Eh  ;n
    je printskin
    cmp [si], 4Eh  ;N
    je printskin   ;the word is skin, print it
    jmp errorcont
    printskin:
    call skin 
    ret
    checkink1:
    cmp [si], 69h  ;i
    je checkink2
    cmp [si], 49h  ;I
    je checkink2
    jmp errorcont
    checkink2:
    inc si
    cmp [si], 6Eh  ;n
    je checkink3
    cmp [si], 4Eh  ;N
    je checkink3
    jmp errorcont
    checkink3:
    inc si
    cmp [si], 6Bh  ;k
    je checkink4   
    cmp [si], 4Bh  ;K
    je checkink4
    jmp errorcont
    checkink4:
    inc si
    cmp [si], 0
    je printink    ;the word is ink, print it
    jmp errorcont
    printink:
    call ink 
    ret
    errorcont:
    call error 
    ret
  checkifcorrect ENDP
  ;------------- 
    
  
  error PROC
    ;proc to print error message
    mov ah, 13h 
    mov bp, offset errormsg
    mov bh, 0
    mov bl, 12
    mov cx, 18
    mov dl, 23
    mov dh, 21
    int 10h 
    ret
  error ENDP
  
  cleararray PROC
    ;proc to clear word array after every use
    mov tmp2, cl
    mov si, offset ar
    mov cl, 4
    loopclear:
    mov [si], 0
    inc si
    loop loopclear
    mov cl, tmp2 
    ret
  cleararray ENDP
  
  errorclear PROC
    ;proc to delete error message after it's shown
    mov ah, 13h
    mov bp, offset clearerror
    mov bh, 0
    mov bl, 0
    mov cx, 18
    mov dl, 23
    mov dh, 21
    int 10h 
    mov ah, 2
    mov dl, 8
    mov dh, 22
    mov bh, 0
    int 10h
    ret
  errorclear ENDP
  
  checkifwon PROC
    ;proc to check if the player guessed all 4 words = win
    mov di, offset scorear
    cmp [di], 1
    je checkcont1
    ret
    checkcont1:
    inc di
    cmp [di], 1
    je checkcont2
    ret
    checkcont2:
    inc di
    cmp [di], 1
    je checkcont3
    ret
    checkcont3:
    inc di
    cmp [di], 1
    je won
    ret
    won:
    mov dl, 100  
    ret
  checkifwon ENDP
    
  ;---Procedures---;
  
  end:

  mov  ax,4c00h
  int  21h  
ends

end start                